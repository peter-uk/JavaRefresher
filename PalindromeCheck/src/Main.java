public class Main {

    public static boolean isPalindromeSB(String word){
        return new StringBuilder(word).reverse().toString().equals(word);
    }

    public static boolean isPalindrome(String word){
        int wordLength = word.length();
        for(int i = (wordLength - 1); i >= 0; i--){
            System.out.println(word.charAt(i));
        }
        return true;
    }

    public static void main(String[] args){
        System.out.println(isPalindromeSB("racecar"));
        System.out.println(isPalindrome("racecar"));
    }
}
