public class Main {
    public static void fizzBuzz(Integer num) {
        for (int i = 0; i < num+1; i++) {
            System.out.println(i + " - "+(i % 3 == 0 ? "Fizz" : "") + (i % 5 == 0 ? "Buzz" : ""));
        }
    }

    public static void main(String[] args) {
        fizzBuzz(25);
    }
}
