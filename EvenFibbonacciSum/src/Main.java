public class Main {
    public static int evenFibonacciSum(int n){
        int totalSum = 0;
        int[] fib = {1,2};

        for(int i = 1;i <= n;i++){
            if(fib[1] % 2 == 0){
                totalSum += fib[1];
            }
            int newfib = fib[1] + fib[0];
            fib[0] = fib[1];
            fib[1] = newfib;
        }
        return totalSum;
    }

    public static void main(String[] args){
        System.out.println(evenFibonacciSum(10));
    }
}
