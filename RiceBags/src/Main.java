public class Main {

    /*
    Provided that you have a given number of
    small rice bags (1 kilo each)
    big rice bags (5 kilos each)
    write a method that returns true if it is possible to make a package with goal kilos of rice.

     */
    public static boolean rice(int big, int small, int goal){
        // Big = 5kg
        // Small - 1kg
        int totalWeight = (big * 5) + small;
        return totalWeight >= goal;
    }

    public static void main(String[] args){
        System.out.println(rice(2,2,13));
    }
}
